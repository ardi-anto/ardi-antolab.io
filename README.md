---
home: true
actionText: Soon... 👋
actionLink: /SOON
features:
- title: Programming
  details: I ❤️ JavaScript, Scala, Golo and Kotlin
- title: CI/CD
  details: I 💚 DevOps, PaaS, GitLab Runners, ...
- title: Tools, methods, ...
  details: I’m a geek, and I 💙 a lot of thinks like IOT, fishing, cooking, ...
footer: MIT Licensed | Copyright © 2018–2019 Philippe Charrière
---